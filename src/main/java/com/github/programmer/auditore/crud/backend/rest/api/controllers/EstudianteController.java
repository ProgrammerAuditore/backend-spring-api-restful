package com.github.programmer.auditore.crud.backend.rest.api.controllers;

import java.util.List;

import com.github.programmer.auditore.crud.backend.rest.api.models.EstudiateModel;
import com.github.programmer.auditore.crud.backend.rest.api.services.EstudianteService;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/estudiantes")
public class EstudianteController {

    private EstudianteService estudianteService;

    public EstudianteController(EstudianteService estudianteService) {
        this.estudianteService = estudianteService;
    }

    // crear un estudiante REST API
    @PostMapping()
    public ResponseEntity<EstudiateModel> saveEstudiante(@RequestBody EstudiateModel estudiante){
        return new ResponseEntity<EstudiateModel>(
            estudianteService.saveEstudiante(estudiante), 
            HttpStatus.CREATED
            );
    }

    // listar estudiantes REST API
    @GetMapping()
    public List<EstudiateModel> getAllEstuantes(){
        return estudianteService.getAllEstudiantes();
    }

    // obtener un estudiante mediante su id REST API
    // http://127.0.0.1:8080/api/v1/estudiantes/1
    @GetMapping("{id}")
    public ResponseEntity<EstudiateModel> getEstudianteById(
        @PathVariable("id") Long estudianteId){

        return new ResponseEntity<EstudiateModel>(
            estudianteService.getEstudiateById(estudianteId), 
            HttpStatus.OK);
            
    }

    // actualizar un estudiante mediante su id REST API
    // http://127.0.0.1:8080/api/v1/estudiantes/1
    @PutMapping("{id}")
    public ResponseEntity<EstudiateModel> updateEstudiante(
    @PathVariable("id") Long estudianteId, 
    @RequestBody EstudiateModel estudiante){

        return new ResponseEntity<EstudiateModel>(
            estudianteService.updateEstudiateModel(estudiante, estudianteId),
            HttpStatus.OK);

    }

    // eliminar un estudiante mediante su id REST API
    // http://127.0.0.1:8080/api/v1/estudiantes/1
    @DeleteMapping("{id}")
    public ResponseEntity<String> deleteEstudiante(@PathVariable("id") Long estudianteId){
        
        // eliminar estudiante de la base de datos
        estudianteService.deleteEstudiante(estudianteId);

        return new ResponseEntity<String>("Estudiante eliminado exitosamente.",  HttpStatus.OK);
        
    }



}
