package com.github.programmer.auditore.crud.backend.rest.api.services;

import java.util.List;

import com.github.programmer.auditore.crud.backend.rest.api.models.EstudiateModel;

public interface EstudianteService {

    EstudiateModel saveEstudiante(EstudiateModel estudiante);
    
    List<EstudiateModel> getAllEstudiantes();

    EstudiateModel getEstudiateById(Long estudianteId);

    EstudiateModel updateEstudiateModel(EstudiateModel estudiante, Long estudianteId);

    void deleteEstudiante(Long estudianteId);

}
