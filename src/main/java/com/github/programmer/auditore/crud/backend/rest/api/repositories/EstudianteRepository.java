package com.github.programmer.auditore.crud.backend.rest.api.repositories;

import com.github.programmer.auditore.crud.backend.rest.api.models.EstudiateModel;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EstudianteRepository extends JpaRepository<EstudiateModel, Long> {

}
