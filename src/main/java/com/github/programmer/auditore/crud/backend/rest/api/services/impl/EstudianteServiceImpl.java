package com.github.programmer.auditore.crud.backend.rest.api.services.impl;

import java.util.List;
// import java.util.Optional;

import com.github.programmer.auditore.crud.backend.rest.api.exceptions.ResourceNotFoundException;
import com.github.programmer.auditore.crud.backend.rest.api.models.EstudiateModel;
import com.github.programmer.auditore.crud.backend.rest.api.repositories.EstudianteRepository;
import com.github.programmer.auditore.crud.backend.rest.api.services.EstudianteService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EstudianteServiceImpl implements EstudianteService {

    @Autowired
    private EstudianteRepository estudianteRepository;
    
    public EstudianteServiceImpl(EstudianteRepository estudianteRepository) {
        this.estudianteRepository = estudianteRepository;
    }

    @Override
    public EstudiateModel saveEstudiante(EstudiateModel estudiante) {
        return estudianteRepository.save(estudiante);
    }

    @Override
    public List<EstudiateModel> getAllEstudiantes() {
        return estudianteRepository.findAll();
    }

    @Override
    public EstudiateModel getEstudiateById(Long estudianteId) {
        
        // Optional<EstudiateModel> estudiante = estudianteRepository.findById(estudianteId);

        // if(!estudiante.isPresent()){
        //     throw new ResourceNotFoundException("Estudiante", "Id", estudianteId);            
        // }

        // return estudiante.get();

        return estudianteRepository.findById(estudianteId)
        .orElseThrow(() -> new ResourceNotFoundException("Estudiante", "Id", estudianteId));
    }

    @Override
    public EstudiateModel updateEstudiateModel(EstudiateModel estudiante, Long estudianteId) {

        // Verificar si el estudiante existe en la base de datos, usando su id
        EstudiateModel existeEstudiante = estudianteRepository.findById(estudianteId)
        .orElseThrow(() -> new ResourceNotFoundException("Estudiate", "Id", estudianteId));

        // Establecer los nuevos datos
        existeEstudiante.setNombre(estudiante.getNombre());
        existeEstudiante.setApellido(estudiante.getApellido());
        existeEstudiante.setCarrera(estudiante.getCarrera());
        existeEstudiante.setEmail(estudiante.getEmail());

        // Guardar los nuevos datos
        estudianteRepository.save(existeEstudiante);


        return existeEstudiante;
    }

    @Override
    public void deleteEstudiante(Long estudianteId) {

        // Verificar si el estudiante existe en la base de datos, usando su id
        estudianteRepository.findById(estudianteId)
        .orElseThrow(() -> new ResourceNotFoundException("Estudiante", "Id", estudianteId));

        estudianteRepository.deleteById(estudianteId);
    }

}
 